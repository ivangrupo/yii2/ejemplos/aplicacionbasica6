<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Formulario is the model behind the contact form.
 */
class Formulario extends Model
{
    public $nombre;
    public $edad;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, edad, are required
            [['nombre', 'edad'], 'required'],
            [['nombre', 'edad'], 'safe'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre del Trabajador',
            'edad' => 'Edad del Trabajador',
        ];
    }

   
}



<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Formulario1 extends Model
{
    public $nombre;
    public $apellidos;
    public $peso;
    public $altura;
    public $poblacion;
    private $valoresPoblacion=[
      "Santander"=>"Santander",
      "Potes"=>"Potes",
      "Laredo"=>"Laredo"
    ];

    public function rules(){
        return [
           [['nombre','peso','altura'],"required"],
            ["peso","integer"],
            ["apellidos","string","max"=>50],
            ["altura","integer","min"=>100,"max"=>250],
            ['poblacion','in','range'=> array_keys($this->getValoresPoblacion())],
           [['nombre','peso','altura','poblacion','apellidos'],"safe"],
        ];
    }
    
    public function attributeLabels(){
        return [
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'peso' => 'Peso',
            'altura' => 'Altura',
            'poblacion' => 'Población',
            
        ];
    }
    
    public function getValoresPoblacion(){
        return $this->valoresPoblacion;
    }
    public function getNombreCompleto(){
        return $this->nombre . " " . $this->apellidos;
    }
    
    public function getIMC(){
        return $this->peso * $this->altura;
    }
    
    
    
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Delegacion;
use app\models\Trabajadores;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        return $this->render('index');
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout(){
        
        return $this->render('about');
    }
    
    public function actionConsulta12(){
        
        return $this->render('consulta12', [
            'modelos' => Trabajadores::find()->all(),
        ]);
    }
    
    public function actionConsulta14(){
        // activeRecord
        $a = Delegacion::find()->all();
        // createCommand
        $b = Yii::$app
                ->db
                ->createCommand("select * from delegacion")
                ->queryAll();
        // Query builder
        $query = new \yii\db\Query();
        $c = $query
                ->select('*')
                ->from('delegacion')
                ->all();
        
        return $this->render('consulta14', [
            'modelos1' => $a,
            'modelos2' => $b,
            'modelos3' => $c,
        ]);
        
    }
    
    public function actionConsulta17(){
        // activeRecord
        $a = Delegacion::find()
                ->select('*')
                ->where(['poblacion' => 'santander'])
                ->all();
        
        // command
        $a1 = Yii::$app
                ->db
                ->createCommand("SELECT * FROM delegacion WHERE poblacion='santander'")
                ->queryAll();
        
        // activeRecord
        $b = Trabajadores::find()
                ->select('*')
                ->where(['delegacion' => 1])
                ->all();
        
        // command
        $b1 = Yii::$app
                ->db
                ->createCommand("SELECT * FROM trabajadores WHERE delegacion=1")
                ->queryAll();
        
        // activeRecord
        $c = Trabajadores::find()
                ->select('*')
                ->orderBy(['nombre' => SORT_ASC])
                ->all();
        
        // command
        $c1 = Yii::$app
                ->db
                ->createCommand("SELECT * FROM trabajadores ORDER BY nombre")
                ->queryAll();
        
        // activeRecord
        $d = Trabajadores::find()
                ->select('*')
                ->where(['fechaNacimiento' => null])
                ->all();
        
        // command
        $d1 = Yii::$app
                ->db
                ->createCommand("SELECT * FROM trabajadores WHERE fechaNacimiento=null")
                ->queryAll();
                
                
//                ->select('ingreso.idingreso', 'ingreso.fecha_hora', 'persona.nombre', 'ingreso.tipo_comprobante', 'ingreso.serie_comprobante', 'ingreso.num_comprobante', 'ingreso.impuesto', 'ingreso.estado', 'SUM("ingreso.cantidad*precio_cantidad") AS total')
//                ->Where(["estado" => 'A'])                                         
//                ->orderBy(['ingreso.idingreso' => SORT_DESC])
        
        return $this->render('consulta17', [
            'modelo1' => $a,
            'modelo2' => $b,
            'modelo3' => $c,
            'modelo4' => $d,
            'modelo5' => $a1,
            'modelo6' => $b1,
            'modelo7' => $c1,
            'modelo8' => $d1,
        ]);
    }
    
    public function actionConsulta20(){
        $datos = [];
        
        // a) activeRecord
        $datos[] = Delegacion::find()
                ->where(["<>", "poblacion", "santander"])
                ->andWhere(["IS NOT", "direccion", null])
                ->asArray() // Sólo para ver los resultados, aunque como no voy a grabar datos no pasa nada
                ->all();
        
//        $datos[] = Delegacion::find()   // Es lo mismo de arriba
//                ->where('poblacion NOT LIKE "santander" and direccion IS NOT NULL')
//                ->asArray() // Sólo para ver los resultados, aunque como no voy a grabar datos no pasa nada
//                ->all();
        
        // a) command
//        $datos[] = Yii::$app
//                ->db
//                ->createCommand("SELECT * FROM delegacion WHERE poblacion NOT LIKE 'santander' and direccion IS NOT NULL")
//                ->queryAll();
        
        // b) activeRecord
        $datos[] = Trabajadores::find()
                ->joinWith('delegacion0')
                ->where(['poblacion' => 'santander'])
                ->asArray() // Sólo para ver los resultados, aunque como no voy a grabar datos no pasa nada
                ->all();
        
        $datos[] = Trabajadores::find() // Hace lo mismo también
                ->joinWith('delegacion0', TRUE, 'INNER JOIN')
                ->all();
        
        $datos[]=Trabajadores::find()
                ->joinWith("delegacionSantander",FALSE,"INNER JOIN")
                ->asArray()
                ->all();
        
        // Optimizada
        $a = Delegacion::find()   // Es lo mismo de arriba
            ->select('id')
            ->where(['poblacion' => 'santander'])
            ->asArray() // Sólo para ver los resultados, aunque como no voy a grabar datos no pasa nada
            ->all();
        
        // Mucho mejor
        ArrayHelper::map($a,'id','id'); // Muy útil para los gridView y listView
                
        $datos[] = Trabajadores::find()
                ->where(['in','delegacion',$a])
                ->asArray()
                ->all();
        
        $datos[]=Trabajadores::find()
                ->where(["IN","delegacion", Delegacion::getDelegacionesId()])
                ->asArray()
                ->all();
        
        
        // b) command
//        $datos[] = Yii::$app
//                ->db
//                ->createCommand("SELECT * FROM trabajadores WHERE delegacion=1")
//                ->queryAll();
        
        // c) activeRecord
        $datos[] = Trabajadores::find()
                ->with('delegacion0')
                ->where('foto IS NOT NULL')
                ->asArray()
                ->all();
        
        // c) command
//        $datos[] = Yii::$app
//                ->db
//                ->createCommand("SELECT * FROM trabajadores ORDER BY nombre")
//                ->queryAll();
        
        // d) activeRecord
        $datos[] = Delegacion::find()
                ->joinWith('trabajadores t')    // Por defecto el joinWith utiliza LEFT JOIN
                ->where(['t.delegacion' => NULL])
                ->asArray()
                ->all();
        
        $delegaciones = Delegacion::find()
                ->with('trabajadores')
                ->asArray()
                ->all();
        
        foreach ($delegaciones as $delegacion) {
            echo var_dump(count($delegacion['trabajadores']));
        }
        
        /*$datos[]=Trabajadores::find()
                ->with('delegacion0')
                ->asArray()
                ->all();*/
        
        /*Trabajadores::find()->all()[0]->delegacion0;*/
        
        
        
        // d) command
//        $datos[] = Yii::$app
//                ->db
//                ->createCommand("SELECT * FROM trabajadores WHERE fechaNacimiento=null")
//                ->queryAll();
                
        
        return $this->render('consulta20', [
            'modelo' => $datos,
        ]);
    }
    
    public function actionConsulta22(){     // static function se utiliza sobre todo cuando no necesite $this
        $datos = [];
        
        $datos[] = Trabajadores::getConsulta1();
        $datos[] = Trabajadores::getConsulta2();
        $datos[] = Trabajadores::getConsulta3();
        $datos[] = Trabajadores::getConsulta4();
                
        
        return $this->render('consulta22', [
            'datos' => $datos,
        ]);
    }
    
}

<?php
namespace app\controllers;

use yii\web\Controller;
use Yii;

use app\models\Formulario;
use app\models\Formulario1;


class FormulariosController extends Controller{
    
    public function actionIndex(){
        $id=Yii::$app->request->post();
        if($id){
            $mensaje=$id;
        }else{
            return $this->redirect(["formularios/formu1"]);
        }
        
        return $this->render("index",[
            //"mensaje"=>Yii::$app->request->post(),
            "mensaje"=>$mensaje,
        ]);
    }
    
    public function actionFormu1(){
       
        return $this->render("formulario");
    }
    
    public function actionFormu2() {
        $model=new Formulario;
        $dato=Yii::$app->request->post();
        if($dato){
            $model->load(Yii::$app->request->post());
            return $this->render("index",[
                "mensaje"=>$model,
            ]);
        }
        
        return $this->render("formulario1",[
            "model"=>$model,
        ]);
    }
    
    public function actionFormu3(){
        $model=new Formulario1();
        $dato=Yii::$app->request->post();
        if($dato){
            $model->load($dato);
            if($model->validate()){
                return $this->render("index1",[
                    "model"=>$model,
                ]);
            }
        }
        return $this->render("formulario2",[
            "model"=>$model,
        ]);
    }


}

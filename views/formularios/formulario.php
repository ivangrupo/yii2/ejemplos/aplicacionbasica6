<?php
    use yii\helpers\Html;
    
    echo Html::beginForm(
        ['formularios/index'],
        'post'
    );
    
    $this->title = 'Formulario 1';
    $this->params['breadcrumbs'][] = $this->title;
    
?>
<div class="form-group">
    <label>Nombre</label>
    <?= Html::input(
            "text", 
            "nombre",
            NULL,
            [
                "class"=>"form-control"
            ]
        );
    ?>
</div>
<div class="form-group">
    <label>Edad</label>
    <?= Html::input(
            "number", 
            "edad",
            NULL,
            [
                "class"=>"form-control"
            ]
        );
    ?>
</div>
    
    <?= Html::submitButton("Enviar",["class"=>"btn btn-primary"]);?>
    <?= Html::endForm();?>



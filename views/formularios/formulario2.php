<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario1 */
/* @var $form ActiveForm */

$this->title = 'Formulario 3';
    $this->params['breadcrumbs'][] = $this->title;

?>
<div class="formulario2">

    <?php $form = ActiveForm::begin([
        'id' => 'formulario2',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>
    <?= $form->errorSummary($model); ?>
        <?= $form->field($model, 'nombre') ?>
        <?= $form->field($model, 'apellidos') ?>
        <?= $form
                ->field($model, 'peso')
                ->textInput([
                    "type"=>"number"
                 ]) ?>
        <?= $form
                ->field($model, 'altura')
                ->textInput([
                    "type"=>"number"
                ]) ?>
        <?= $form
                ->field($model, 'poblacion')
                ->dropDownList(
                    $model->getValoresPoblacion(),[
                        'prompt'=>"Seleccione una población"
                    ]); ?>
    
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario2 -->

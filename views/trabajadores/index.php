<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="bodycontent">
        
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    
    </div>
    

    <p>
        <?= Html::a('Añadir Trabajador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,  // Búsqueda
        'columns' => [

            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
            [
              'attribute'=>'foto',
              'format'=>'raw',
              'value'=>function($dato){
                return Html::img("@web/imgs/$dato->foto",[
                    'class'=>'img-responsive',
                    'width'=>'75',
                ]);
              },  
            ],
            'delegacion',
            [
                'attribute'=>'nombredelegacion',
                'label'=>'Nombre',
                'value'=>'delegacion0.nombre',
            ],
            [
                'attribute'=>'poblacion',
                'label'=>'Población',
                'value'=>'delegacion0.poblacion',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delegacion} {delete}',
                'buttons'=>[
                    'delegacion'=>function($url,$model,$key){
                        //return Html::a('<span class="glyphicon glyphicon-home"></span>',$url);
                        return Html::a('<span class="glyphicon glyphicon-home"></span>',[
                            'delegacion/view',"id"=>$model->delegacion,
                        ]);
                    }
                ]
            ],
               
        ],
    ]); ?>
</div>
